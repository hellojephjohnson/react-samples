import React from 'react'

export function About() {
  return (
    <div className="container">
      {/* height is just for place holder */}
      <div className="row" style={{ height: "100vh" }}>
          <div className="col-12 text-center align-self-center">
            <h1>About Us</h1>
          </div>
      </div>
    </div>
  )
}
