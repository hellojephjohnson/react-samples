import React from 'react'
import Carousel from '../../components/carousel/carousel'

export function Home() {
  return (
    <Carousel />
  )
}
