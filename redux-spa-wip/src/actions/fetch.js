export const fetchData = id => {
  return function (dispatch) {
    dispatch({
      type: "FETCH_DATA_FULFILLED",
      payload: id
    });
  }
}