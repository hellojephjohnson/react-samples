import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import counterReducer from './counter'
import fetchReducer from './fetch'

const rootReducer = (history) => combineReducers({
  count: counterReducer,
  fetch: fetchReducer,
  router: connectRouter(history)
})

export default rootReducer
