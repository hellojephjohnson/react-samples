const initialArray = {fetch:[], error: null}

const fetchData = (state = initialArray, action) => {
  switch (action.type) {
    case "FETCH_DATA_FULFILLED":
      return { fetch: action.payload }
    case "FETCH_DATA_REJECTED": 
      return { error: action.payload }
    default:
      return state
  }
}
export default fetchData