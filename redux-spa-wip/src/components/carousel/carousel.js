import React, { Fragment, Component} from 'react'
import { bindActionCreators } from "redux";
import { connect } from 'react-redux'
import classNames from "classnames";
import { increment, decrement, indexCounter } from '../../actions/counter'
import { fetchData } from '../../actions/fetch'
import data from '../../assets/data/output.json'

const Arrows = ({className, spanName, onClick, carouselId, dataSlide, ariaHidden, text }) => {
  return (
    <a className={className} onClick={onClick} href={carouselId} role="button" data-slide={dataSlide}>
      <span className={spanName} aria-hidden={ariaHidden}/>
      <span className="sr-only">{text}</span>
    </a>
  )
}

class Carousel extends Component {

  componentDidMount() {
    this.props.fetchData(data);
  }
  
  _onDotClick = index => {
    this.props.indexCounter(index);
    
  };

  _prev = (e, count, fetch) => {
    e.preventDefault();
    const newLength = fetch.length-1
    return count === 0 ? this.props.indexCounter(newLength) : this.props.decrement();;
  };

  _next = (e, count, fetch) => {
    e.preventDefault();
    const newLength = fetch.length-1
    return count === newLength ? this.props.indexCounter(0) : this.props.increment();
  };

  _renderSlides = (fetch, count) => {
    return (
      <div className="carousel-inner">
      {fetch.map((item, index) => {
          //const randomImage = fetch[Math.floor(Math.random() * fetch.length)].url
          let isShown = index === count;
          let componentNames = classNames(
            "carousel-item",
            { "active": isShown },
          );
          return (
            <div className="carousel-item" key={index} className={componentNames}>
              <img
                aria-hidden={!isShown}
                src={item.url}
                alt=""
              />
            </div>
          )
        })
      }
      </div>
    );
  }

  _renderArrows = (fetch, count) => {
    return (
      <Fragment>
        <Arrows 
          className="carousel-control-prev" 
          text="Previous" 
          dataSlide="prev"
          carouselId="#carousel"
          spanName="carousel-control-prev-icon"
          ariaHidden="true"
          onClick={e => this._prev(e, count, fetch)}
        />
        <Arrows 
          className="carousel-control-next" 
          text="Next" 
          dataSlide="next"
          carouselId="#carousel"
          spanName="carousel-control-next-icon"
          ariaHidden="true"
          onClick={e => this._next(e, count, fetch)}
        />
      </Fragment>
    );
  }

  _renderDots = (fetch, count) => {
    return (
      <ol className="carousel-indicators">
        {fetch.map((item, index) => {
          let isShown = index === count;
          let componentNames = classNames({
            "active": isShown
          });
          let id = `carousel${index}`;
          return (
            <li id={id} data-target="#carousel" data-slide-to={index} className={componentNames} key={index} onClick={e => this._onDotClick(index)} />
            );
          })
        }
      </ol>
    );
  }

  render() {
    const { count, fetch } = this.props;
    return (
      <div 
        id="carousel" 
        className="carousel slide carousel-fade" 
        data-ride="carousel" 
        data-interval="5000">
          {this._renderDots(fetch, count)}
          {this._renderSlides(fetch, count)}
          {this._renderArrows(fetch, count)}
      </div>
    );
  }
}

const mapStateToProps = state => ({ count: state.count, fetch: state.fetch.fetch });
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { increment, decrement, indexCounter, fetchData },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Carousel)
