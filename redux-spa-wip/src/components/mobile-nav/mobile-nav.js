import React from "react";
import classNames from "classnames";
import { bool, string, func } from "prop-types";

export function MobileNav ({ onClick, targetId, active, className }) {
	
	const mobileNavClass = classNames(
		"navbar-toggler",
    { "collapsed": active },
    className
	);
	
  return (
     <button className={mobileNavClass} 
				type="button" 
				data-toggle="collapse" 
				data-target={targetId} 
				aria-controls="navbarCollapse" 
				aria-expanded={active ? true : false}
				aria-label="Toggle navigation"
				onClick={onClick}
			>
			<span className="navbar-toggler-icon"></span>
		</button>
  );
}

MobileNav.propTypes = {
  /** Function triggered by onClick */
	onClick: func.isRequired,
	/** Used to toggle Active state */
	active: bool,
	/** ID used for identifying component nav ID. */
  targetId: string,
  /** Any extra CSS classes for the component */
  className: string,
};

