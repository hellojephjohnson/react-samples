import React, { PureComponent } from "react";
import { arrayOf, shape, string, element } from "prop-types";
import { NavLink, Link } from 'react-router-dom'
import classNames from "classnames";
import { MobileNav } from "../mobile-nav/index";
import Logo from '../../assets/images/logo-white.svg';

const DefaultLogo = () => (
  <Link to={"/"} className="navbar-brand"><Logo /></Link>
);

export class Nav extends PureComponent {

  state = {
    targetId: "#navbarCollapse",
    active: true
  }

  _renderLinks = (links) => {
    return (
      <ul className="navbar-nav">
        {links.map(link => (
          <li className="nav-item" key={link.text}>
            <NavLink to={link.url} className="nav-link" title={link.text} exact={link.exact}>{link.text}</NavLink>
          </li>
        ))}
      </ul>
    )
  }

  _toggleMobile = () => {
    this.setState(prevState => ({
      active: !prevState.active
    }));
  }

  render() {
      const { links, customLogo, className } = this.props
      const { targetId, active } = this.state

      const navBarClass = classNames(
        "navbar navbar-expand-md",
        className
      ); 

      const navToggleClass = classNames(
        "collapse navbar-collapse justify-content-end",
        { "show": !active }
      );

      return (
        <nav className={navBarClass}>
          {customLogo ? customLogo : <DefaultLogo />}
          <MobileNav targetId={targetId} active={active} onClick={this._toggleMobile} />
          <div className={navToggleClass} id={targetId}>
            {links && this._renderLinks(links)}
          </div>
      </nav>
      )
  }
}


Nav.defaultProps = {
  links: []
};

Nav.propTypes = {
  /* Links to display next to copyright notice */
  links: arrayOf(
    shape({
      text: string.isRequired,
      url: string.isRequired
    })
  ),
  /* Custom logo to be used for white label solutions. Expects a React component */
  customLogo: element
};
