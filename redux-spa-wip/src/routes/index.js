import React, {Fragment} from 'react'
import { Route, Switch } from 'react-router'
import { Nav } from '../components/nav/index'
import { Home } from '../pages/home/index'
import { About } from '../pages/about/index'
import { Contact } from '../pages/contact/index'
import { NoMatch } from '../pages/no-match/index'

const routes = (
  <Fragment>
    <header>
      <Nav
        className="navbar-dark fixed-top bg-dark"
        links={[
          { text: "Home", url: "/", exact: true },
          { text: "About", url: "/about" },
          { text: "Contact Us", url: "/contact" }
        ]}
      />
    </header>
    <main role="main">
      <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route component={NoMatch} />
      </Switch>
    </main>
  </Fragment>
)

export default routes
