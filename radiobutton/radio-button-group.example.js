import React, { Component } from "react";
import { RadioButton } from "../radiobutton";
import { RadioButtonGroup } from "./radio-button-group";
import { text } from "@storybook/addon-knobs";

export let name = "RadioButton - Group";

export class RadioGroupExample extends Component {
  constructor() {
    super();
    this.state = {
      myError: true
    };
  }

  _onGroupChange = () => {
    this.setState({ myError: null });
  };

  render() {
    const name = "test-radiogroup";
    const className = "row";
    return (
      <RadioButtonGroup
        name={name}
        className={className}
        onChange={this._onGroupChange}
        status={this.state.myError && RadioButtonGroup.STATUS.ERROR}
        errorMessage={text("Error Message", "UH OH! You must choose one.")}
      >
        <RadioButton
          label={text("RadioButton one", "Radio one...")}
          value="option 1"
          className="col-xs-12"
        />
        <RadioButton
          label={text("RadioButton two", "Radio two...")}
          value="option 2"
          className="col-xs-12"
        />
        <RadioButton
          label={text("RadioButton three", "Radio three...")}
          value="option 3"
          className="col-xs-12"
        />
      </RadioButtonGroup>
    );
  }
}

export const Example = () => {
  return <RadioGroupExample />;
};

const RadioButtonExports = [RadioButton, RadioButtonGroup];

export { RadioButtonExports as Component };
