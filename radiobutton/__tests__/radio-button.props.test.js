import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { RadioButton } from "../radio-button";

const onClickSpy = jest.fn();

describe("RadioButton component", () => {
  describe("Render", () => {
    test("should render radiobutton label", () => {
      const props = {
        label: "My RadioButton"
      };
      const { getByText } = render(<RadioButton {...props} />);
      getByText(props.label);
    });
  });

  describe("Functional", () => {
    test("should fire onChange prop when radio-button is clicked", () => {
      const { getByTestId } = render(
        <RadioButton
          testId="spark-radio-button-click"
          onChange={onClickSpy}
          label="Click me!"
        />
      );

      const radioClick = getByTestId("spark-radio-button-click", {
        selector: "input"
      });

      fireEvent.click(radioClick);
      expect(onClickSpy).toHaveBeenCalledTimes(1);
    });
  });
});
