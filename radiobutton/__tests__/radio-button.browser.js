import React from "react";
import { render } from "jest-puppeteer-react";

import { RadioButton } from "../index";

describe.only("RadioButton - Visual Regression", () => {
  const puppeteerOptions = {
    viewport: { width: 200, height: 100 }
  };

  test("renders a radio-button with label and in 'unchecked' status", async () => {
    await render(<RadioButton label={"My RadioButton"} />, puppeteerOptions);

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "radio-button-with-label-unchecked",
      failureThreshold: "0.25",
      failureThresholdType: "percent"
    });
  });

  test("renders a radio-button in 'checked' status", async () => {
    await render(
      <RadioButton label={"My RadioButton"} checked={true} />,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "radio-button-checked",
      failureThreshold: "0.25",
      failureThresholdType: "percent"
    });
  });

  test("renders a radio-button in 'disabled' status", async () => {
    await render(
      <RadioButton label={"My RadioButton"} disabled={true} />,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "radio-button-disabled",
      failureThreshold: "0.25",
      failureThresholdType: "percent"
    });
  });
});
