import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import { RadioButton } from "../index";
import { RadioButtonGroup } from "../radio-button-group";

afterEach(cleanup);

const onClickSpy = jest.fn();
let radiobuttonText = "RadioButton Render";

describe("RadioButtonGroup component", () => {
  describe("Render", () => {
    test("should render 3 radio-button children", () => {
      const { getAllByLabelText } = render(
        <RadioButtonGroup>
          <RadioButton label={radiobuttonText} />
          <RadioButton label={radiobuttonText} />
          <RadioButton label={radiobuttonText} />
        </RadioButtonGroup>
      );

      const inputNode = getAllByLabelText(radiobuttonText, {
        selector: "input"
      });

      expect(inputNode.length).toBe(3);
    });
    describe("Error Message", () => {
      test("should render error message", () => {
        const props = {
          status: RadioButtonGroup.STATUS.ERROR,
          errorMessage: "UH OH! You must choose one."
        };

        const { getByText } = render(
          <RadioButtonGroup {...props}>
            <RadioButton label={radiobuttonText} />
            <RadioButton label={radiobuttonText} />
            <RadioButton label={radiobuttonText} />
          </RadioButtonGroup>
        );
        getByText(props.errorMessage);
      });

      test("should not show error message", () => {
        const props = {
          errorMessage: "UH OH! You must choose one."
        };

        const { queryByText } = render(
          <RadioButtonGroup {...props}>
            <RadioButton label={radiobuttonText} />
            <RadioButton label={radiobuttonText} />
            <RadioButton label={radiobuttonText} />
          </RadioButtonGroup>
        );

        expect(queryByText(props.errorMessage)).toBeFalsy();
      });
    });
  });

  describe("Functional", () => {
    test("onClick prop should fire 3 times", () => {
      const { getAllByLabelText } = render(
        <RadioButtonGroup onChange={onClickSpy}>
          <RadioButton label={radiobuttonText} />
          <RadioButton label={radiobuttonText} />
          <RadioButton label={radiobuttonText} />
        </RadioButtonGroup>
      );

      const inputNode = getAllByLabelText(radiobuttonText, {
        selector: "input"
      });

      inputNode.map(node => fireEvent.click(node));

      expect(onClickSpy).toHaveBeenCalledTimes(inputNode.length);
    });
  });
});
