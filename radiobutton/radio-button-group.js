import React from "react";
import { string, func, node, oneOf } from "prop-types";
import classNames from "classnames";

class RadioButtonGroup extends React.Component {
  _onChangeHandler = e => {
    this.props.onChange(e.currentTarget);
  };

  render() {
    const { className, status, errorMessage, name, testId } = this.props;
    const fieldSetClasses = classNames("spark-radio-group", className);
    const attrs = {
      className: fieldSetClasses
    };

    switch (status) {
      case RadioButtonGroup.STATUS.ERROR:
        attrs["data-error"] = "true";
        break;
      case RadioButtonGroup.STATUS.WARNING:
        attrs["data-warning"] = "true";
        break;
      case RadioButtonGroup.STATUS.SUCCESS:
        attrs["data-success"] = "true";
        break;
      case RadioButtonGroup.STATUS.INFO:
        attrs["data-info"] = "true";
        break;
      default:
        break;
    }

    const children = React.Children.map(this.props.children, child => {
      return typeof (child.type === "function") &&
        child.type.name === "RadioButton"
        ? React.cloneElement(child, { name, onChange: this._onChangeHandler })
        : child;
    });

    return (
      <fieldset {...attrs} data-testid={testId}>
        {status && (
          <span className="spark-radio-group__message">{errorMessage}</span>
        )}
        {children}
      </fieldset>
    );
  }
}

RadioButtonGroup.STATUS = {
  ERROR: "error",
  WARNING: "warning",
  SUCCESS: "success",
  INFO: "info"
};

RadioButtonGroup.propTypes = {
  /** Checkbox/HTML elements */
  children: node.isRequired,
  /** Any extra CSS classes for the checkbox group */
  className: string,
  /** Message for errors */
  errorMessage: string,
  /** Status for data-errors, data-warnings, data-success, data-info */
  status: oneOf([...Object.values(RadioButtonGroup.STATUS), null]),
  /** Function to be called when each checkbox is clicked */
  onChange: func,
  /** Pass in string for Name */
  name: string,
  /** Testing Key for Jest */
  testId: string
};

export { RadioButtonGroup };
