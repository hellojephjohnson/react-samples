import React from "react";
import { RadioButton } from "../radiobutton";
import { text, boolean } from "@storybook/addon-knobs";

export let name = "RadioButton - Default";

export let Example = () => {
  const showOptional = boolean("Disabled", true, false, false);
  return (
    <fieldset className="row">
      <RadioButton
        label={text("RadioButton one", "Radio one...")}
        name="test-radio"
        value="option 1"
        className="col-xs-12"
      />
      <RadioButton
        label={text("RadioButton two", "Radio two...")}
        name="test-radio"
        value="option 2"
        className="col-xs-12"
      />
      <RadioButton
        label={text("RadioButton three", "Radio three...")}
        name="test-radio"
        value="option 3"
        disabled={showOptional}
        checked={showOptional === false ? boolean("Checked", false) : null}
        className="col-xs-12"
      />
    </fieldset>
  );
};

export { RadioButton as Component };
