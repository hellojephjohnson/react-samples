import React from "react";
import { string, func, bool } from "prop-types";
import classNames from "classnames";
import uniqueId from "lodash.uniqueid";

export function RadioButton({
  className,
  label,
  id,
  name,
  onClick,
  testId,
  checked,
  disabled,
  value,
  ...rest
}) {
  const checkboxId = id || uniqueId("spark-radio_");
  const componentClass = classNames("spark-radio", className);

  const inputProps = {
    className: "spark-radio__input",
    id: checkboxId,
    name: name,
    onClick: onClick,
    value: value,
    defaultChecked: checked,
    disabled: disabled
  };

  return (
    <label className={componentClass} data-testid={testId}>
      <input type="radio" {...inputProps} {...rest} />
      <span className="spark-radio__box" />
      <span className="spark-label">{label}</span>
    </label>
  );
}

RadioButton.displayName = "RadioButton";

RadioButton.propTypes = {
  /** Any extra CSS classes for the component */
  className: string,
  /** Name for the input field */
  name: string,
  /** Any extra CSS id for the component */
  id: string,
  /** Text displayed with the Checkbox */
  label: string.isRequired,
  /** Function to trigger when value changes */
  onClick: func,
  /** Value for input */
  value: string,
  /** Testing Key for Jest */
  testId: string,
  /** HTML `checked` attribute */
  checked: bool,
  /** HTML `disabled` attribute */
  disabled: bool
};
