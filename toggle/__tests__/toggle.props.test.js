import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { Toggle } from "../index";
import { ToggleGroup } from "../toggle-group";

const onClickSpy = jest.fn();
const togglebuttonText = "Toggle Render";
const toggleName = "test-toggle";

describe("Toggle Render", () => {
  describe("Render", () => {
    test("should render 2 toggle children nested in toggle-group", () => {
      const { getAllByLabelText } = render(
        <ToggleGroup name={toggleName}>
          <Toggle label={togglebuttonText} />
          <Toggle label={togglebuttonText} />
          <Toggle label={togglebuttonText} />
        </ToggleGroup>
      );

      const inputNode = getAllByLabelText(togglebuttonText, {
        selector: "input"
      });

      expect(inputNode.length).toBe(3);
    });

    describe("Toggle Switch", () => {
      test("should render Toggle switch outside of group", () => {
        const props = {
          label: "flip",
          ariaLabelledBy: "labelID",
          switchToggle: true
        };

        const { queryByText } = render(<Toggle {...props} />);
        queryByText(props.label);
      });
    });
  });

  describe("Functional", () => {
    test("onChange prop should fire 1 time", () => {
      render(
        <Toggle
          label={togglebuttonText}
          onChange={onClickSpy}
          ariaLabelledBy={"labelID"}
          switchToggle
        />
      );

      const toggleNode = document.querySelector(".spark-toggle__input");

      fireEvent.click(toggleNode);
      expect(onClickSpy).toHaveBeenCalledTimes(1);
    });

    test("onClick prop should fire 4 times", () => {
      const { getAllByLabelText } = render(
        <ToggleGroup onClick={onClickSpy} name={toggleName}>
          <Toggle label={togglebuttonText} />
          <Toggle label={togglebuttonText} />
          <Toggle label={togglebuttonText} />
        </ToggleGroup>
      );

      const toggleNode = getAllByLabelText(togglebuttonText, {
        selector: "input"
      });

      toggleNode.map(node => fireEvent.click(node));
      expect(onClickSpy).toHaveBeenCalledTimes(4);
    });
  });
});
