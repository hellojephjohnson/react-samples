import React from "react";
import { render } from "jest-puppeteer-react";
import { Toggle } from "../index";
import { ToggleGroup } from "../toggle-group";

describe("Toggle - Visual Regression", () => {
  const puppeteerOptions = {
    viewport: { width: 300, height: 200 }
  };

  test("renders a toggle default with one checked item", async () => {
    await render(
      <ToggleGroup name="toggle-test">
        <Toggle label="flip" checked />
        <Toggle label="flop" />
      </ToggleGroup>,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-default-checked"
    });
  });

  test("renders a toggle default disabled", async () => {
    await render(
      <ToggleGroup name="toggle-test">
        <Toggle label="flip" disabled />
        <Toggle label="flop" disabled />
      </ToggleGroup>,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-default-disabled"
    });
  });

  test("renders a toggle condensed with one checked item", async () => {
    await render(
      <ToggleGroup name="toggle-test" condensed>
        <Toggle label="flip" checked />
        <Toggle label="flop" />
      </ToggleGroup>,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-condensed-checked"
    });
  });

  test("renders a toggle condensed with one disabled", async () => {
    await render(
      <ToggleGroup name="toggle-test" condensed>
        <Toggle label="flip" disabled />
        <Toggle label="flop" disabled />
      </ToggleGroup>,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-condensed-disabled"
    });
  });

  test("renders a toggle icon with one checked item", async () => {
    await render(
      <ToggleGroup name="toggle-test">
        <Toggle iconName="spark-icon-lock-close" ariaLabel="Lock" checked />
        <Toggle iconName="spark-icon-lock-open" ariaLabel="unlock" />
      </ToggleGroup>,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-icon-checked"
    });
  });

  test("renders a toggle icon disabled", async () => {
    await render(
      <ToggleGroup name="toggle-test">
        <Toggle iconName="spark-icon-lock-close" ariaLabel="Lock" disabled />
        <Toggle iconName="spark-icon-lock-open" ariaLabel="unlock" disabled />
      </ToggleGroup>,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-icon-disabled"
    });
  });

  test("renders a toggle switch-toggle clicked", async () => {
    await render(
      <Toggle
        className="col-xs-4 col-sm-2"
        ariaLabelledBy={"labelID"}
        switchToggle
      />,
      puppeteerOptions
    );

    await page.evaluate(() => {
      document.querySelector(".spark-toggle__input").click();
    });

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-switch-clicked"
    });
  });

  test("renders a toggle switch-toggle disabled", async () => {
    await render(
      <Toggle
        className="col-xs-4 col-sm-2"
        ariaLabelledBy={"labelID"}
        switchToggle
        disabled
      />,
      puppeteerOptions
    );

    const screenshot = await page.screenshot();
    expect(screenshot).toMatchImageSnapshot({
      customSnapshotIdentifier: "toggle-switch-disabled"
    });
  });
});
