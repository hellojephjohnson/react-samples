import React, { Fragment } from "react";
import { string, bool, func } from "prop-types";
import classNames from "classnames";
import uniqueId from "lodash.uniqueid";

export function Toggle({
  className,
  id,
  disabled,
  checked,
  switchToggle,
  condensed,
  iconName,
  label,
  name,
  onClick,
  onChange,
  value,
  testId,
  ariaLabel,
  ariaLabelledBy,
  ...rest
}) {
  const toggleSwitchId = id || uniqueId("spark-toggle_");
  const componentClass = classNames(
    { "spark-toggle--xs": condensed },
    "spark-toggle",
    className
  );

  const inputProps = {
    className: "spark-toggle__input",
    type: "radio",
    disabled: disabled,
    iconname: iconName,
    name: name,
    switchtoggle: switchToggle,
    onClick: onClick,
    value: value,
    defaultChecked: checked
  };

  const renderSwitchToggle = (toggleSwitchId, disabled) => {
    return (
      <Fragment>
        <label
          id={toggleSwitchId}
          className="spark-toggle-switch"
          aria-labelledby={ariaLabelledBy}
        >
          <input
            className="spark-toggle__input"
            type="checkbox"
            disabled={disabled}
            onChange={onChange}
            {...rest}
          />
          <span className="spark-toggle-switch__handle"></span>
          <span className="spark-toggle-switch__track"></span>
        </label>
      </Fragment>
    );
  };

  const renderIcon = (componentClass, toggleSwitchId, inputProps) => {
    return (
      <label id={toggleSwitchId} className={componentClass}>
        <input {...inputProps} aria-label={ariaLabel} />
        <span className="spark-label">
          <i className={iconName}></i>
        </span>
      </label>
    );
  };

  const renderDefault = (componentClass, toggleSwitchId, inputProps) => {
    return (
      <label id={toggleSwitchId} className={componentClass}>
        <input {...inputProps} {...rest} />
        <span className="spark-label">{label}</span>
      </label>
    );
  };

  const renderToggle = (
    componentClass,
    toggleSwitchId,
    inputProps,
    disabled,
    testId,
    onClick
  ) => {
    if (switchToggle) {
      return renderSwitchToggle(toggleSwitchId, disabled, testId, onClick);
    } else if (iconName) {
      return renderIcon(componentClass, toggleSwitchId, inputProps);
    } else {
      return renderDefault(componentClass, toggleSwitchId, inputProps);
    }
  };
  return renderToggle(
    componentClass,
    toggleSwitchId,
    inputProps,
    disabled,
    testId,
    onClick
  );
}

Toggle.propTypes = {
  /** Any extra CSS classes for the component */
  className: string,
  /** Boolean to use Icon the component */
  iconName: string,
  /** ariaLabelledBy String to use the Switch component */
  ariaLabelledBy: function(props) {
    if (props.switchToggle && !props.ariaLabelledBy && !props.disabled) {
      return new Error(
        "Failed prop type: The prop `ariaLabelledBy` is required when using the `switchToggle prop. Please refer to Spark EDL documentation: `http://sabrespark.com/ui/toggle.html#accessibility`"
      );
    }
  },
  ariaLabel: function(props) {
    if (props.iconName && !props.ariaLabel && !props.disabled) {
      return new Error(
        "Failed prop type: The prop `ariaLabel` is required when using the `iconName prop. Please refer to Spark EDL documentation: `http://sabrespark.com/ui/toggle.html#accessibility`"
      );
    }
  },
  /** Boolean to use switch the component */
  switchToggle: bool,
  /** Additional onClick Function for the component */
  onClick: func,
  /** Additional onChange Function for the component */
  onChange: func,
  /** Boolean to show checked for the component */
  checked: bool,
  /** Boolean to disable the component */
  disabled: bool,
  /** ID for the component */
  id: string,
  /** Value for input */
  value: string,
  /** label for the component */
  label: string
};
