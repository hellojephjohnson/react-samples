import React, { Fragment } from "react";
import { Toggle } from "./toggle";
import { ToggleGroup } from "./toggle-group";

export let name = "Toggle - Icon";

export let Example = () => {
  return (
    <Fragment>
      <h3>Icon Toggle</h3>
      <ToggleGroup
        name="test-toggle-icon-b"
        onClick={e => console.log(e)}
        role="group"
      >
        <Toggle iconName="spark-icon-lock-close" ariaLabel="Lock" checked />
        <Toggle iconName="spark-icon-lock-open" ariaLabel="Unlock" />
      </ToggleGroup>
    </Fragment>
  );
};

const ToggleExports = [Toggle, ToggleGroup];

export { ToggleExports as Component };
