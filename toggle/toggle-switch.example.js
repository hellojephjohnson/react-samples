import React, { Fragment } from "react";
import { Toggle } from "./toggle";

export let name = "Toggle - Switch";

export let Example = () => {
  return (
    <Fragment>
      <h3>Switch Toggle</h3>
      <p id="labelID" className="spark-label spark-mar-0">
        Switch Label for Demo Purposes
      </p>
      <Toggle
        className="col-xs-4 col-sm-2"
        ariaLabelledBy={"labelID"}
        onClick={e => console.log(e)}
        switchToggle
      />
      <br />
      <Toggle className="col-xs-4 col-sm-2" switchToggle disabled />
    </Fragment>
  );
};

export { Toggle as Component };
