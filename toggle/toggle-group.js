import React from "react";
import { string, bool, node, func } from "prop-types";
import classNames from "classnames";

export const ToggleGroup = ({
  condensed,
  className,
  name,
  children,
  onClick,
  testId,
  ...rest
}) => {
  const componentGroupClass = classNames("row spark-toggle-group", className);

  const childrenItems = children.map((child, key) => {
    return React.cloneElement(child, {
      condensed: condensed,
      name,
      onClick,
      key
    });
  });

  return (
    <div className={componentGroupClass} data-testid={testId} {...rest}>
      {childrenItems}
    </div>
  );
};

ToggleGroup.displayName = "Toggle Group";

ToggleGroup.propTypes = {
  /** Any extra CSS classes for the component */
  className: string,
  /** Function to be called when each checkbox is clicked */
  onClick: func,
  /** Checkbox/HTML elements */
  children: node.isRequired,
  /** Each Name needs a unique indentifier name inside a toggle-group component */
  name: string.isRequired,
  /** Boolean to use condensed buttons in the component */
  condensed: bool,
  /** Testing Key for Jest */
  testId: string
};
