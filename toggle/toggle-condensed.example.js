import React, { Fragment } from "react";
import { Toggle } from "./toggle";
import { ToggleGroup } from "./toggle-group";
import { text } from "@storybook/addon-knobs";

export let name = "Toggle - Condensed";

export let Example = () => {
  return (
    <Fragment>
      <h3>Condensed Toggle</h3>
      <ToggleGroup
        name="test-toggle-condensed"
        onClick={e => console.log(e)}
        condensed
        role="group"
      >
        <Toggle
          className="col-xs-4 col-sm-2"
          label={text("Flip", "Flip")}
          checked
        />
        <Toggle className="col-xs-4 col-sm-2" label={text("Flop", "Flop")} />
      </ToggleGroup>

      <ToggleGroup
        className="spark-mar-t-1"
        name="test-toggle-condensed-a"
        onClick={e => console.log(e)}
        condensed
        role="group"
      >
        <Toggle
          className="col-xs-4 col-sm-2"
          label={text("Flip", "Flip")}
          checked
        />
        <Toggle className="col-xs-4 col-sm-2" label={text("Flop", "Flop")} />
        <Toggle className="col-xs-4 col-sm-2" label={text("Flow", "Flow")} />
      </ToggleGroup>

      <ToggleGroup
        className="spark-mar-t-1"
        name="test-toggle-condensed-b"
        onClick={e => console.log(e)}
        condensed
        role="group"
      >
        <Toggle
          className="col-xs-4 col-sm-2"
          label={text("Flip", "Flip")}
          disabled
        />
        <Toggle
          className="col-xs-4 col-sm-2"
          label={text("Flop", "Flop")}
          disabled
        />
      </ToggleGroup>
    </Fragment>
  );
};

const ToggleExports = [Toggle, ToggleGroup];

export { ToggleExports as Component };
