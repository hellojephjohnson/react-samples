import React, { Fragment } from "react";
import { Slider } from "../slider";

export let name = "Slider - Basic";

export let Example = () => {
  return (
    <Fragment>
      <div className="col-xs-12 spark-mar-b-2">
        <h3 id="indeterminate-slider" className="toc-hover--header">
          Indeterminate Slider
        </h3>
        <Slider className="spark-slider" label="Slider Label" />
        <Slider className="spark-slider" label="Slider Label" disable />

        <h3 id="alternate-indeterminate-slider" className="toc-hover--header">
          Alternate Indeterminate Slider
        </h3>
        <Slider
          className="spark-slider spark-slider--secondary"
          label="Slider Label"
        />
        <Slider
          className="spark-slider spark-slider--secondary"
          label="Slider Label"
          disable
        />

        <h3 id="single-value-slider" className="toc-hover--header">
          Single Value Slider
        </h3>
        <Slider
          className="spark-slider--input"
          label="Slider Label"
          defaultValue={5}
          minValue={-20}
          maxValue={40}
        />
        <Slider
          className="spark-slider--input"
          label="Slider Label"
          step={5}
          defaultValue={5}
          minValue={-20}
          maxValue={40}
        />
        <Slider
          className="spark-slider--input"
          label="Slider Label"
          step={5}
          defaultValue={23}
          minValue={-20}
          maxValue={40}
          disable
        />

        <h3 id="single-integrated-value-example" className="toc-hover--header">
          Single Integrated Value Example
        </h3>
        <Slider
          className="spark-slider--integrated"
          label="Slider Label"
          defaultValue={4}
          minValue={-20}
          maxValue={40}
        />
        <Slider
          className="spark-slider--integrated"
          label="Slider Label"
          defaultValue={4}
          minValue={-20}
          maxValue={40}
          disable
        />
        <h3 id="slider-error-example" className="toc-hover--header">
          Slider Error Example
        </h3>
        <Slider
          className="spark-slider--input"
          label="Slider Label"
          maxValue={50}
          dataError
        />
        <Slider
          className="spark-slider--integrated"
          label="Slider Label"
          maxValue={50}
          dataError
        />
        <h3 id="multiple-values-slider-1" className="toc-hover--header">
          Multiple Values Slider
        </h3>
        <h4 id="option-1-2" className="toc-hover--header">
          Option 1
        </h4>
        <Slider
          className="spark-range-slider"
          label="Slider Label"
          rangeValue={[10, 90]}
          rangeSlider
        />
        <Slider
          className="spark-range-slider"
          label="Slider Label"
          rangeValue={[10, 90]}
          rangeSlider
          step={0.5}
        />
        <Slider
          className="spark-range-slider"
          label="Slider Label"
          rangeValue={[10, 90]}
          rangeSlider
          disable
        />
      </div>
      <h4 id="option-1-2" className="toc-hover--header">
        Option 2
      </h4>
      <Slider
        className="spark-range-slider spark-slider--secondary"
        label="Slider Label"
        minValue={-40}
        maxValue={100}
        rangeValue={[-10, 90]}
        rangeSlider
      />
      <Slider
        className="spark-range-slider spark-slider--secondary"
        label="Slider Label"
        minValue={-40}
        maxValue={100}
        rangeValue={[-10, 90]}
        rangeSlider
        step={5}
      />
      <h3 id="multiple-range-slider" className="toc-hover--header">
        Multiple Range Slider
      </h3>
      <Slider
        className="spark-range-slider"
        label="Slider Label"
        minValue={-40}
        maxValue={100}
        rangeValue={[-20, 20, 90]}
        rangeSlider
        multi
      />
      <Slider
        className="spark-range-slider"
        label="Slider Label"
        minValue={-40}
        maxValue={100}
        rangeValue={[-20, 20, 90]}
        rangeSlider
        multi
        step={5}
      />
      <Slider
        className="spark-range-slider"
        label="Slider Label"
        minValue={-40}
        maxValue={100}
        rangeValue={[-20, 20, 90]}
        rangeSlider
        multi
        disable
      />
    </Fragment>
  );
};

export { Slider as Component };
