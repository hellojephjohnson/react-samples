import React, { PureComponent, Fragment } from "react";
import {
  string,
  bool,
  oneOfType,
  func,
  number,
  object,
  array
} from "prop-types";
import uniqueId from "lodash.uniqueid";
import classNames from "classnames";
import modulo from "../../utils/modulo";

import RcSlider from "rc-slider";
import "rc-slider/assets/index.css";

export class Slider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.defaultValue,
      rangeValue: this.props.rangeSlider && [
        this.props.rangeValue[0],
        this.props.rangeValue[1],
        this.props.rangeValue[2]
      ],
      disable: this.props.disable,
      dataError: this.props.dataError
    };
    this.sliderId = props.id || uniqueId("spark-slider_");
    this.sparkSliderProps = {};
    this.state.dataError && (this.sparkSliderProps["data-error"] = true);
    this.sliderContainer = React.createRef();

    setTimeout(() => {
      const rcSlider = this.sliderContainer.current.querySelector(".rc-slider");
      rcSlider.classList.remove("rc-slider");

      const rcSliderRail = this.sliderContainer.current.querySelector(
        ".rc-slider-rail"
      );
      rcSliderRail.classList.add("spark-slider__track");
      rcSliderRail.classList.remove("rc-slider-rail");

      const rcSliderTrack = this.sliderContainer.current.querySelector(
        ".rc-slider-track"
      );
      rcSliderTrack.classList.add("spark-slider__track-fill");
      rcSliderTrack.classList.remove("rc-slider-track");

      const rcSliderTrack2 = this.sliderContainer.current.querySelector(
        ".rc-slider-track-2"
      );

      const sparkSliderTrack = this.sliderContainer.current.querySelector(
        ".spark-slider__track"
      );

      const sparkSliderFill = this.sliderContainer.current.querySelector(
        ".spark-slider__track-fill"
      );

      const myItems = [sparkSliderFill, rcSliderTrack2];
      const trackItems = ["rc-slider-track", "rc-slider-track-2"];

      sparkSliderTrack.append(...myItems);

      sparkSliderFill.classList.remove("rc-slider-track-1");
      rcSliderTrack2 &&
        rcSliderTrack2.classList.add("spark-slider__track-fill");
      rcSliderTrack2 && rcSliderTrack2.classList.remove(...trackItems);
    }, 0);
  }

  _getValue() {
    return this.state.value;
  }

  _onSliderChange = value => {
    const { disable, dataError } = this.props;
    !disable && dataError && value === 0
      ? (this.sparkSliderProps["data-error"] = true)
      : (this.sparkSliderProps = {});
    this.setState({ value });
  };

  _onInputChange = (e, index) => {
    /*eslint complexity: ["error", 25]*/
    const { disable, rangeSlider, multi, dataError, step } = this.props;
    let numberValue = parseFloat(e.target.value);
    numberValue = step ? numberValue - modulo(numberValue, step) : numberValue;
    console.log(numberValue);
    !disable && index === 0 && dataError && numberValue === 0
      ? (this.sparkSliderProps["data-error"] = true)
      : (this.sparkSliderProps = {});
    this.setState({
      rangeValue: rangeSlider && [
        numberValue < this.state.rangeValue[1]
          ? numberValue
          : !numberValue == "" && this.state.rangeValue[1],
        this.state.rangeValue[1],
        multi && this.state.rangeValue[2]
      ],
      value: numberValue
    });
    index === 1 &&
      this.setState({
        rangeValue: rangeSlider && [
          this.state.rangeValue[0],
          numberValue >= this.state.rangeValue[2]
            ? this.state.rangeValue[2]
            : numberValue > this.state.rangeValue[0]
            ? numberValue
            : !numberValue == "" && this.state.rangeValue[0],
          multi && this.state.rangeValue[2]
        ]
      });
    index === 2 &&
      this.setState({
        rangeValue: rangeSlider && [
          this.state.rangeValue[0],
          this.state.rangeValue[1],
          multi && numberValue < this.state.rangeValue[1]
            ? this.state.rangeValue[1]
            : numberValue
          //multi && numberValue <= this.state.rangeValue[1] ? this.state.rangeValue[1] : !numberValue == "" && this.state.rangeValue[1] ? this.state.rangeValue[1] : numberValue
        ]
      });
  };

  _onRangeChange = rangeValue => {
    const { disable } = this.props;
    !disable && this.setState({ rangeValue });
  };

  render() {
    const {
      className,
      label,
      id,
      ariaLabel,
      title,
      minValue,
      maxValue,
      step,
      dots,
      isValueVisible,
      onBeforeChange,
      onAfterChange,
      railStyle,
      trackStyle,
      handleStyle,
      pushable,
      rangeSlider,
      dataError,
      allowCross,
      count
    } = this.props;

    const Handle = RcSlider.Handle;
    const Range = RcSlider.Range;

    const defaultClasses = classNames(
      { "all-disabled": this.state.disable },
      className
    );

    const handleClasses = classNames(
      { disabled: this.state.disable },
      "spark-slider__handle"
    );

    const handle = props => {
      const { value, offset, index } = props;
      return (
        <Handle
          className={handleClasses}
          role="slider"
          value={value}
          aria-valuenow={value}
          aria-valuetext={value}
          aria-labelledby="slider-label-12"
          data-value={value}
          offset={offset}
          id={id}
          key={`${this.props.label}${index}`}
        />
      );
    };

    const inputRangeElement = index => {
      const { minValue, maxValue, title, ariaLabel, multi } = this.props;
      const newIndex = multi && index + 1;
      return (
        <Fragment key={`${this.props.label}${index}`}>
          <span className="spark-range-slider__input-divider" />
          <input
            type="number"
            min={minValue}
            max={maxValue}
            value={
              !multi
                ? this.state.rangeValue[1]
                : this.state.rangeValue[newIndex]
            }
            step={step}
            aria-label={ariaLabel}
            disabled={this.state.disable}
            title={
              newIndex === 1 && multi
                ? "Middle"
                : newIndex > 1 && multi
                ? "End"
                : title
            }
            onChange={e =>
              multi
                ? this._onInputChange(e, newIndex)
                : this._onInputChange(e, 1)
            }
          />
        </Fragment>
      );
    };

    const inputRangeElements = () => {
      const { multi } = this.props;
      return !multi
        ? inputRangeElement()
        : Array(2)
            .fill("")
            .map((a, b) => inputRangeElement(b));
    };

    return (
      <div
        className={defaultClasses}
        {...this.sparkSliderProps}
        ref={this.sliderContainer}
      >
        {label && (
          <Fragment>
            <span className="spark-label">{label}</span>
            <input
              type="number"
              min={minValue}
              max={maxValue}
              value={rangeSlider ? this.state.rangeValue[0] : this.state.value}
              step={step}
              aria-label={ariaLabel}
              disabled={this.state.disable}
              title={rangeSlider ? "Start" : title}
              id={id}
              onChange={e => this._onInputChange(e, 0)}
            />
            {rangeSlider && inputRangeElements()}
          </Fragment>
        )}
        {isValueVisible && (
          <span className="slider-value">{this._getValue()}</span>
        )}
        {rangeSlider ? (
          <Range
            className="spark-slider__controls"
            value={
              this.props.rangeValue[2]
                ? [...this.state.rangeValue]
                : [this.state.rangeValue[0], this.state.rangeValue[1]]
            }
            onChange={this._onRangeChange}
            min={minValue}
            max={maxValue}
            step={step}
            count={count}
            pushable={pushable}
            handle={handle}
            allowCross={allowCross}
          />
        ) : (
          <RcSlider
            className="spark-slider__controls"
            min={minValue}
            max={maxValue}
            //defaultValue={this._getValue()}
            value={this._getValue()}
            step={step}
            dots={dots}
            onChange={this._onSliderChange}
            onBeforeChange={onBeforeChange}
            onAfterChange={onAfterChange}
            pushable={pushable}
            trackStyle={trackStyle}
            handleStyle={handleStyle}
            railStyle={railStyle}
            handle={handle}
          />
        )}
        {dataError && (
          <span className="spark-slider__message">
            Please complete the required field
          </span>
        )}
      </div>
    );
  }
}

Slider.defaultProps = {
  minValue: 0,
  maxValue: 100,
  defaultValue: 0,
  allowCross: false
};

Slider.propTypes = {
  /**  Used to define a string that labels the current element */
  label: string,
  /** Any extra CSS classes for the component */
  className: string,
  /** Boolean to identify wheter slider is disabled or not */
  disable: bool,
  /** Min value of Slider */
  minValue: number,
  /** Max value of Slider */
  maxValue: number,
  /** Default value of Slider */
  defaultValue: oneOfType([array, number]),
  /** Steps, how Slider should move */
  step: number,
  /** Boolean to specify if line with Slider would be dotted or not */
  dots: bool,
  /** Boolean to specify if value with progress is visible or not */
  value: bool,
  /** onBeforeChange will be triggered when ontouchstart or onmousedown is triggered. */
  onBeforeChange: func,
  /** onAfterChange will be triggered when ontouchend or onmouseup is triggered. */
  onAfterChange: func,
  /** The style used for the track base color. */
  railStyle: object,
  /** The style used for track. (both for slider(Object) and range(Array of Object), the array will be used for multi track following element order) */
  trackStyle: object,
  /** The style used for handle. (both for slider(Object) and range(Array of Object), the array will be used for multi handle following element order) */
  handleStyle: object,
  /** pushable could be set as true to allow pushing of surrounding handles when moving a handle. When set to a number, the number will be the minimum ensured distance between handles.  */
  pushable: oneOfType([bool, number])
};
